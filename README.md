﻿Remote PhoneSQL  
==========================================  


Hero Management  
==========================================  
<b>PhoneSQL#HERO SETUP $NEWPASS</b>  
		<i>Adiciona um novo hero para executar comandos no dispositivo. [DONE]</i>  
  
<b>PhoneSQL#HERO ADD $NUMBER</b>  
		<i>Adiciona outro número de telefone para se tornar hero diretamente. [DONE]</i>  
  
<b>PhoneSQL#HERO DELETE $NUMBER</b>  
		<i>Remove um número de telefone da lista de heros. [DONE]</i>  
  
<b>PhoneSQL#HERO LIST</b>  
		<i>Lista todos os números de telefone que podem executar comandos no dispositivo. [DONE]</i>  
  
<b>PhoneSQL#HERO PASSWORD $NEWPASS</b>  
		<i>Troca a senha para se tornar um hero e avisa os outros heros da nova senha. [DONE]</i>  
  
<b>PhoneSQL#HERO PREFIX $NEWPREFIX</b>  
		<i>Troca o prefixo para executar comandos e avisa os outros heros do novo prefixo. [DONE]</i>  
  

Location Management  
==========================================  
<b>PhoneSQL#LOCATION TRACK</b>  
		<i>Inicia a localização do aparelho (enviará uma vez a msg pro usuário). [DONE]</i>  
  
<b>PhoneSQL#LOCATION TRACK EACH $TIME</b>  
		<i>Inicia a localização do aparelho a cada X minutos. [DONE]</i>  
  
<b>PhoneSQL#LOCATION TRACK SEND $NUMBER</b>  
		<i>Envia a localização para o número informado em vez do que está mandando a mensagem. [DONE]</i>  
  
<b>PhoneSQL#LOCATION TRACK POST $URL</b>  
		<i>Posta a localização para a URL X em vez de mandar mensagem.</i>  
  
<b>PhoneSQL#LOCATION TRACK DISABLE</b>  
		<i>Desabilita a localização em andamento. [DONE]</i>  
  
<b>PhoneSQL#LOCATION TRACK DISABLE ALL</b>  
		<i>Desabilita todos os pedidos de localização. [DONE]</i>  
  
<b>PhoneSQL#LOCATION TRACK DISABLE $NUMBER</b>  
		<i>Desabilita o pedido de localização realizado pelo número informado. [DONE]</i>  
  


SMS Management  
==========================================  
<b>PhoneSQL#SMS DELETE ALL</b>  
		<i>Deleta todas as mensagens (entrada e saída) SMS do dispositivo. [DONE]</i>  
  
<b>PhoneSQL#SMS DELETE $NUMBER</b>  
		<i>Deleta todas as mensagens enviadas ou recebidas do número informado. [DONE]</i>  
  
<b>PhoneSQL#SMS ROUTE ALL</b>  
		<i>Envia uma cópia de todas as mensagens recebidas. [DONE]</i>  
		<i>Observação: Nao executa em mensagens recebidas por outros heroes.</i>  
  
<b>PhoneSQL#SMS ROUTE ALL DISABLE</b>  
		<i>Desativa o envio de todas as mensagens recebidas. [DONE]</i>  
  
<b>PhoneSQL#SMS ROUTE $NUMBER1</b>  
		<i>Envia uma cópia de todas as mensagens recebidas do número informado. [DONE]</i>  
  
<b>PhoneSQL#SMS ROUTE $NUMBER1 DISABLE</b>  
		<i>Desativa o envio de todas as mensagens recebidas do número informado. [DONE]</i>  
  
<b>PhoneSQL#SMS SEND $NUMBER $TEXT</b>  
		<i>Envia uma mensagem para o número informado com o texto recebido. [DONE]</i>  
  


Call Management  
==========================================  
<b>PhoneSQL#CALL $NUMBER</b>  
		<i>Inicia uma ligação para o número informado. [DONE]</i>  
  
<b>PhoneSQL#CALL $NUMBER LOUDER</b>  
		<i>Inicia uma ligação para o número informado e ativa o auto-falante. [DONE]</i>  
  
<b>PhoneSQL#CALL ACCEPT $NUMBER</b>  
		<i>Aceita automaticamente as ligações recebidas do número informado. [DONE]</i>  
  
<b>PhoneSQL#CALL ACCEPT $NUMBER LOUDER</b>  
		<i>Aceita automaticamente as ligações recebidas do número informado e ativa o auto-falante. [DONE]</i>  
		<i>Observaçao: O telefone vai atender com o auto-falante ativo, porém em alguns celulares ele não indica isso.</i>  
  
<b>PhoneSQL#CALL ACCEPT $NUMBER DISABLE</b>  
		<i>Desabilita o recebimento automatico de ligações do número informado. [DONE]</i>  
  

Contact Management  
==========================================  
<b>PhoneSQL#CONTACT GET $NAME</b>  
		<i>Retorna a lista de contatos e seus telefones que contenham o nome informado. [DONE]</i>  
  
<b>PhoneSQL#CALL DELETE $NAME</b>  
		<i>Deleta o contato informado da lista de contatos.</i>  
  


Block Management  
==========================================  
<b>PhoneSQL#BLOCK ALL</b>  
		<i>Bloqueia todas as comunicações do dispositivo. [DONE]</i>  
  
<b>PhoneSQL#BLOCK ALL DISABLE</b>  
		<i>Desbloqueia todas as comunicações do dispositivo. [DONE]</i>  
  
<b>PhoneSQL#BLOCK ALL $NUMBER</b>  
		<i>Bloqueia todas as comunicações do dispositivo com o número informado. [DONE]</i>  
  
<b>PhoneSQL#BLOCK ALL $NUMBER DISABLE</b>  
		<i>Desbloqueia todas as comunicações do dispositivo com o número informado. [DONE]</i>  
  
<b>PhoneSQL#BLOCK SMS ALL</b>  
		<i>Bloqueia todas as comunicações via SMS do dispositivo. [DONE]</i>  
  
<b>PhoneSQL#BLOCK SMS ALL DISABLE</b>  
		<i>Desbloqueia todas as comunicações via SMS do dispositivo. [DONE]</i>  
  
<b>PhoneSQL#BLOCK SMS ALL $NUMBER</b>  
		<i>Função indisponível no Android. Não é possível interceptar SMS enviados, apenas recebidos. [DONE]</i>  
  
<b>PhoneSQL#BLOCK SMS ALL $NUMBER DISABLE</b>  
		<i>Função indisponível no Android. Não é possível interceptar SMS enviados, apenas recebidos. [DONE]</i>  
  
<b>PhoneSQL#BLOCK SMS TO $NUMBER</b>  
		<i>Função indisponível no Android. Não é possível interceptar SMS enviados, apenas recebidos. [DONE]</i>  
  		
<b>PhoneSQL#BLOCK SMS TO $NUMBER DISABLE</b>  
		<i>Função indisponível no Android. Não é possível interceptar SMS enviados, apenas recebidos. [DONE]</i>  
  
<b>PhoneSQL#BLOCK SMS FROM $NUMBER</b>  
		<i>Bloqueia todas as mensagens recebidas do número informado. [DONE]</i>  
  
<b>PhoneSQL#BLOCK SMS FROM $NUMBER DISABLE</b>  
		<i>Desbloqueia todas as mensagens recebidas do número informado. [DONE]</i>  
  
<b>PhoneSQL#BLOCK CALL ALL</b>  
		<i>Bloqueia o dispositivo de receber ou realizar chamadas. [DONE]</i>  
  
<b>PhoneSQL#BLOCK CALL ALL DISABLE</b>  
		<i>Desbloqueia o dispositivo de receber ou realizar chamadas. [DONE]</i>  
  
<b>PhoneSQL#BLOCK CALL ALL $NUMBER</b>  
		<i>Bloqueia o dispositivo de realizar ou receber ligações do número informado. [DONE]</i>  
  
<b>PhoneSQL#BLOCK CALL ALL $NUMBER DISABLE</b>  
		<i>Desbloqueia o dispositivo de realizar ou receber ligações do número informado. [DONE]</i>  
  
<b>PhoneSQL#BLOCK CALL TO $NUMBER</b>  
		<i>Bloqueia o dispositivo de realizar ligações para o número informado. [DONE]</i>  
  
<b>PhoneSQL#BLOCK CALL TO $NUMBER DISABLE</b>  
		<i>Desbloqueia o dispositivo de realizar ligações para o número informado. [DONE]</i>  
  
<b>PhoneSQL#BLOCK CALL FROM $NUMBER</b>  
		<i>Bloqueia o dispositivo de receber ligações do número informado. [DONE]</i>  
  
<b>PhoneSQL#BLOCK CALL FROM $NUMBER DISABLE</b>  
		<i>Desbloqueia o dispositivo de receber ligações do número informado. [DONE]</i>  
  


Alert Management  
==========================================  
<b>PhoneSQL#ALERT BOOT</b>  
		<i>Envia uma mensagem quando o celular é ligado. [DONE]</i>  
  
<b>PhoneSQL#ALERT BOOT DISABLE</b>  
		<i>Desabilita o envio de uma mensagem quando o celular é ligado. [DONE]</i>  
  
<b>PhoneSQL#ALERT SIM</b>  
		<i>Envia uma mensagem quando o SIM é trocado. [DONE]</i>
  
<b>PhoneSQL#ALERT SIM DISABLE</b>  
		<i>Desabilita o envio de uma mensagem quando o SIM é trocado. [DONE]</i>