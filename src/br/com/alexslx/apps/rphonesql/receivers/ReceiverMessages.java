/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.rphonesql.receivers;

import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import br.com.alexslx.apps.rphonesql.AppConfig;
import br.com.alexslx.apps.rphonesql.models.Contact;
import br.com.alexslx.apps.rphonesql.models.Hero;
import br.com.alexslx.apps.rphonesql.services.MessageParserService;
import br.com.alexslx.apps.rphonesql.utils.L;
import br.com.alexslx.apps.rphonesql.utils.PhoneUtils;

public class ReceiverMessages extends BroadcastReceiver
{

	@Override
	public void onReceive(Context context, Intent intent)
	{
		Bundle extras 				= intent.getExtras();
		Object[] pdus 				= (Object[])extras.get("pdus");
		StringBuilder msgBuilder 	= new StringBuilder();
		boolean abortBroadcast 		= false;
		String senderNumber  		= "";
		String senderMessage 		= "";
		String messagePrefix		= AppConfig.getInstance(context).getMsgPrefix().toUpperCase();

		this.abortBroadcast();
		
		for (Object pdu : pdus)
		{
			SmsMessage smsHandler = SmsMessage.createFromPdu((byte[]) pdu);
			if(senderNumber.isEmpty())
				senderNumber = smsHandler.getDisplayOriginatingAddress();
			
			msgBuilder.append(smsHandler.getDisplayMessageBody());
		}
		
		senderMessage = msgBuilder.toString().trim();
		
		if( senderMessage.toUpperCase().startsWith(messagePrefix) )
		{
			abortBroadcast = true;
			senderMessage  = senderMessage.substring( messagePrefix.length() );
			
			Intent i = new Intent(context, MessageParserService.class);
			i.putExtra("number", senderNumber);
			i.putExtra("message", senderMessage);
			context.startService(i);
		}
		else
		{
			Contact c = Contact.getContactByNumber(senderNumber);
			
			if( !Hero.isHero(senderNumber) )
			{
				if( AppConfig.getInstance(context).getRouteAllCount() > 0 )
				{
					List<Hero> heroList = Hero.listAll(Hero.class);
					for(Hero h: heroList)
					{
						if(h.getSettings().getRouteAllMessages())
						{
							PhoneUtils.routeMessage(context, senderMessage, h.getNumber(), senderNumber);
						}
					}
				}
				
				if( AppConfig.getInstance(context).isBlocked("SMS") )
				{
					L.d("[Global] Message from " + senderNumber + " has been blocked!");
					abortBroadcast = true;
				}
				
				if( c != null && c.getSettings().isIncommingMsgBlocked() )
				{
					L.d("[Contact] Message from " + senderNumber + " has been blocked!");
					abortBroadcast = true;
				}
			}
			
			if( c != null && c.getSettings().getRouteNumbersCount() > 0)
			{
				List<String> numberList = c.getSettings().getRouteNumbers();
				for(String number : numberList)
				{
					PhoneUtils.routeMessage(context, senderMessage, number, senderNumber);
				}
			}
		}

		if (!abortBroadcast)
			this.clearAbortBroadcast();
	}
}
