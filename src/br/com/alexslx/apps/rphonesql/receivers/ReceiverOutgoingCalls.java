/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.rphonesql.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import br.com.alexslx.apps.rphonesql.AppConfig;
import br.com.alexslx.apps.rphonesql.models.Contact;
import br.com.alexslx.apps.rphonesql.models.Hero;
import br.com.alexslx.apps.rphonesql.utils.L;

public class ReceiverOutgoingCalls extends BroadcastReceiver
{

	@Override
	public void onReceive(Context context, Intent intent)
	{
		String receivedNumber = getResultData() != null ? getResultData() : intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
		if (receivedNumber == null)
			return;

		if( !Hero.isHero(receivedNumber) )
		{
			if( AppConfig.getInstance(context).isBlocked("CALL") )
			{
				L.d("[Global] Blocked outgoing call to " + receivedNumber);
				setResultData(null);
			}
			
			Contact c = Contact.getContactByNumber(receivedNumber);
			if( c != null && c.getSettings().isOutgoingCallBlocked() )
			{
				L.d("[Contact] Blocked outgoing call to " + receivedNumber);
				setResultData(null);
			}
		}
	}
}
