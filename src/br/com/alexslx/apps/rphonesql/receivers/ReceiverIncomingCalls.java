/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.rphonesql.receivers;

import java.lang.reflect.Method;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import br.com.alexslx.apps.rphonesql.AppConfig;
import br.com.alexslx.apps.rphonesql.models.Contact;
import br.com.alexslx.apps.rphonesql.models.Hero;
import br.com.alexslx.apps.rphonesql.services.IncomingCallService;
import br.com.alexslx.apps.rphonesql.utils.L;

public class ReceiverIncomingCalls extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent)
	{
		Bundle extras = intent.getExtras();
		if (extras == null)
			return;

		String state = extras.getString(TelephonyManager.EXTRA_STATE);
		if (state == null)
			return;
		
		try
		{
			if (state.equals(TelephonyManager.EXTRA_STATE_RINGING))
			{
				String callerNumber = extras.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
				Contact c 			= Contact.getContactByNumber(callerNumber);
				
				// Rejects just works for non hero.
				if(!Hero.isHero(callerNumber))
				{
					if( AppConfig.getInstance(context).isBlocked("CALL") ||	(c != null && c.getSettings().isIncommingCallBlocked()) )
					{
						L.d("Call from " + callerNumber + " has been blocked!");
						declineCall(context);
						Intent i = new Intent(context, IncomingCallService.class);
	                    context.startService(i);
					}
				}
				
				// Auto accept
				if( c != null && c.getSettings().isIncommingCallAccept() )
				{
					L.d("Answering a call");
					answerCall(context);
					if (c.getSettings().isIncommingCallLouder())
						AppConfig.getInstance(context).setLouderInNextCall(true);
				}
			}
			else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK))
			{
				if (AppConfig.getInstance(context).getLouderInNextCall() == true)
				{
					L.d("Activating louder setting...");

					Thread.sleep(7000);

					AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
					audioManager.setMode(AudioManager.MODE_IN_CALL);
					audioManager.setSpeakerphoneOn(true);

					AppConfig.getInstance(context).setLouderInNextCall(false);
				}
			}
			else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE))
			{
				// Finish outgoing call
			}
		}
		catch(Exception e)
		{
			//L.d("Error: " + e.getMessage());
		}
	}

	private void answerCall(Context context)
	{
		Intent buttonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
		buttonIntent.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
		context.sendOrderedBroadcast(buttonIntent, "android.permission.CALL_PRIVILEGED");
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void declineCall(Context context)
	{
		AudioManager audioManager	= (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        int oldRingerMode			= audioManager.getRingerMode();
        audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        
		try
		{
			String serviceManagerName 			= "android.os.ServiceManager";
			String serviceManagerNativeName 	= "android.os.ServiceManagerNative";
			String telephonyName 				= "com.android.internal.telephony.ITelephony";

			Class telephonyClass 				= Class.forName(telephonyName);
			Class serviceManagerClass			= Class.forName(serviceManagerName);
			Class serviceManagerNativeClass		= Class.forName(serviceManagerNativeName);
			Class telephonyStubClass			= telephonyClass.getClasses()[0];
			
			Method getService 					= serviceManagerClass.getMethod("getService", String.class);
			Method tempInterfaceMethod 			= serviceManagerNativeClass.getMethod("asInterface", IBinder.class);

			Binder tmpBinder 					= new Binder();
			tmpBinder.attachInterface(null, "fake");
			
			Object serviceManagerObject  = tempInterfaceMethod.invoke(null, tmpBinder);
			IBinder retbinder 			 = (IBinder)getService.invoke(serviceManagerObject, "phone");
			Method serviceMethod 		 = telephonyStubClass.getMethod("asInterface", IBinder.class);
			Object telephonyObject  	 = serviceMethod.invoke(null, retbinder);
			Method telephonyEndCall 	 = telephonyClass.getMethod("endCall");

			telephonyEndCall.invoke(telephonyObject);
		}
		catch (Exception e)
		{
			L.d("[ITelephony] Error trying to end a call.");
		}
		
		audioManager.setRingerMode(oldRingerMode);
	}
}
