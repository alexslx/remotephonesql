package br.com.alexslx.apps.rphonesql.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import br.com.alexslx.apps.rphonesql.utils.PhoneUtils;

public class ReceiverShutdown extends BroadcastReceiver
{
	@Override
	public void onReceive(Context context, Intent intent)
	{
		PhoneUtils.onShutdownRequest(context);
	}
}
