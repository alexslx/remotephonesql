package br.com.alexslx.apps.rphonesql.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import br.com.alexslx.apps.rphonesql.services.AppBootService;
import br.com.alexslx.apps.rphonesql.services.AppSIMService;

public class ReceiverAppBoot extends BroadcastReceiver
{
	@Override
	public void onReceive(Context context, Intent intent)
	{
		if ( intent.getAction().equals("android.intent.action.BOOT_COMPLETED") )
		{
			context.startService(new Intent(context, AppBootService.class));
			context.startService(new Intent(context, AppSIMService.class));
		}
	}
}
