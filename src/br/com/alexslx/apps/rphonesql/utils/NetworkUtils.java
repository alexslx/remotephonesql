package br.com.alexslx.apps.rphonesql.utils;

import java.lang.reflect.Method;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;

public class NetworkUtils
{
	private WifiManager			wifiManager;
	private ConnectivityManager	mdataManager;
	private static NetworkUtils	_instance	= null;

	private NetworkUtils(Context context)
	{
		this.wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		try
		{
			this.mdataManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		}
		catch (Exception e)
		{
			L.e("Couldn't get an instance of ConnectivityManager");
		}
	}

	public static NetworkUtils getNetworkUtils(Context context)
	{
		if (_instance == null)
			_instance = new NetworkUtils(context);
		
		return _instance;
	}

	public boolean isWifiEnabled()
	{
		return wifiManager.isWifiEnabled();
	}

	public void turnOnWifi(boolean state)
	{
		wifiManager.setWifiEnabled(state);
	}

	public void enableMobileData(boolean state)
	{
		try
		{
			Method dataMtd = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);
			dataMtd.setAccessible(true);
			dataMtd.invoke(this.mdataManager, state);
		}
		catch (Exception e)
		{
			L.e("Couldn't enable mobile data");
		}
	}
}
