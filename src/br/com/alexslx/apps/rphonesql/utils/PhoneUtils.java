package br.com.alexslx.apps.rphonesql.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.telephony.SmsManager;
import br.com.alexslx.apps.rphonesql.AppConfig;
import br.com.alexslx.apps.rphonesql.R;
import br.com.alexslx.apps.rphonesql.models.AppLocation;
import br.com.alexslx.apps.rphonesql.models.Hero;
import br.com.alexslx.apps.rphonesql.services.LocationService;

public class PhoneUtils
{
	private static int MIN_MATCH		= 7;
	public static int SMS_MAX_LENGTH	= 160;
	
	public static String getCurrentDate()
	{
		return new SimpleDateFormat("dd/MM").format(new Date());
	}
	
	public static String getCurrentTime()
	{
		return new SimpleDateFormat("HH:mm").format(new Date());
	}
	
	public static String getCurrentDateTime()
	{
		return new SimpleDateFormat("dd/MM - HH:mm").format(new Date());
	}
	
	public static boolean compareLoosely(String a, String b)
	{
		if (a == null || b == null)
			return a == b;
		
		if (a.length() == 0 || b.length() == 0)
			return false;
		
		if (a.length() > MIN_MATCH)
			a = a.substring( a.length() - MIN_MATCH );
		
		if (b.length() > MIN_MATCH)
			b = b.substring( b.length() - MIN_MATCH );
		
		return a.equalsIgnoreCase(b);
	}
	
	public static void onBootCompleted(Context context)
	{
		String message = String.format( context.getString(R.string.alert_boot_message), AppConfig.getInstance(context).getLastShutdown(), PhoneUtils.getCurrentDateTime() );
		
		List<Hero> heroList = Hero.listAll(Hero.class);
		for(Hero hero : heroList)
		{
			if( hero.getSettings().getAlertOnBoot() )
				hero.sendTextMessage(message);
		}
	}
	
	public static void onShutdownRequest(Context context)
	{
		AppConfig.getInstance(context).setShutdown( PhoneUtils.getCurrentDateTime() );
	}

	public static void onSIMChanged(Context context, String oldSIMSerialNumber, String newSIMSerialNumber)
	{
		oldSIMSerialNumber 	= oldSIMSerialNumber.isEmpty() ? "N/A" : oldSIMSerialNumber;
		String message		= String.format( context.getString(R.string.alert_sim_message), oldSIMSerialNumber, newSIMSerialNumber);
		
		List<Hero> heroList = Hero.listAll(Hero.class);
		for(Hero hero : heroList)
		{
			if( hero.getSettings().getAlertOnSIMChange() )
				hero.sendTextMessage(message);
		}
	}
	
	public static void routeMessage(Context context, String msg, String heroNumber, String fromNumber)
	{
		String message = fromNumber + ":\n" + msg;
		PhoneUtils.sendMessage(heroNumber, message);
	}
	
	public static void sendMessage(String number, String message)
	{
		SmsManager smsManager 	= SmsManager.getDefault();
		
		if( message.length() > SMS_MAX_LENGTH)
		{
			L.d("Sending a long sms");
			ArrayList<String> parts	= smsManager.divideMessage( message );
			smsManager.sendMultipartTextMessage(number, null, parts, null, null);
		}
		else
		{
			L.d("Sending a short sms");
			smsManager.sendTextMessage(number, null, message, null, null);
		}
	}
	
	public static void deleteAllSMS(Context context)
	{
		L.d("Deleting all SMS conversations...");
		
		Uri uri = Uri.parse("content://sms");
		ContentResolver contentResolver = context.getContentResolver();
		Cursor cursor = contentResolver.query(uri, null, null, null, null);
		
		while (cursor.moveToNext())
		{
			long thread_id 		= cursor.getLong(1);
			Uri thread 			= Uri.parse("content://sms/conversations/" + thread_id);
			contentResolver.delete(thread, null, null);
		}
		
		L.d("ALL SMS conversations has been deleted.");
	}
	
	public static void deleteSMSByNumber(Context context, String number)
	{
		L.d("Deleting SMS conversations from " + number);
		
		Uri uri = Uri.parse("content://sms");
		ContentResolver contentResolver = context.getContentResolver();
		Cursor cursor = contentResolver.query(uri, null, null, null, null);
		
		while (cursor.moveToNext())
		{
			long thread_id 			= cursor.getLong(1);
			String contactNumber	= cursor.getString(2);
			if( PhoneUtils.compareLoosely(contactNumber, number) )
			{
				Uri thread = Uri.parse("content://sms/conversations/" + thread_id);
				contentResolver.delete(thread, null, null);
			}
		}
		
		L.d("SMS conversations from " + number + " has been deleted.");
	}

	public static void newLocationListener(Context context, AppLocation location, boolean newLocation)
	{
		AppConfig settings	= AppConfig.getInstance(context);
		int locationCount 	= settings.getLocationCount();
		long minInterval	= settings.getLocationInterval();
		
		if(minInterval == 0 || minInterval > location.getInterval())
		{
			minInterval = location.getInterval();
			settings.setLocationInterval(minInterval);
			if(!newLocation)
			{
				//TODO: Better way to do this
				Intent i = new Intent(context, LocationService.class);
                context.stopService(i);
                context.startService(i);
			}
		}
		
		if(newLocation)
		{
			locationCount++;
			settings.setLocationCount(locationCount);
		}
		
		if( !LocationService.isRunning() || (newLocation && locationCount == 1))
		{
			L.d("New LocationService service started!");
			Intent i = new Intent(context, LocationService.class);
            context.startService(i);
		}
	}
	
	public static void deleteLocationListener(Context context)
	{
		AppConfig settings	= AppConfig.getInstance(context);
		int locationCount 	= settings.getLocationCount();
		if( locationCount > 0)
			locationCount--;
		
		if(locationCount == 0)
		{
			L.d("No more tracks, stopping the service...");
			settings.setLocationInterval(0);
			
			Intent i = new Intent(context, LocationService.class);
            context.stopService(i); 
		}
	}
}
