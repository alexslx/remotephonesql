/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.rphonesql.services;

import java.util.ArrayList;
import java.util.Arrays;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import br.com.alexslx.apps.alc.LicenseUtils;
import br.com.alexslx.apps.rphonesql.R;
import br.com.alexslx.apps.rphonesql.commands.CommandEnum;
import br.com.alexslx.apps.rphonesql.commands.CommandManager;
import br.com.alexslx.apps.rphonesql.models.Hero;
import br.com.alexslx.apps.rphonesql.utils.L;

public class MessageParserService extends IntentService
{
	public MessageParserService()
	{
		super("ServiceMessageParser");
	}
	
	@Override
	protected void onHandleIntent(Intent intent)
	{
		Context context 		= getBaseContext();
		String number			= intent.getExtras().getString("number").trim();
		String message			= intent.getExtras().getString("message").trim();
		String argv[]			= message.split("\\s");
		String command			= argv[0];
		
		ArrayList<String> argList = new ArrayList<String>( Arrays.asList(argv) );
		argList.remove(0);
		
		Hero hero = Hero.getHeroByNumber(number);
		if( hero == null )
		{
			//TODO: Remove hard coded string
			if( command.equalsIgnoreCase(CommandEnum.HERO.name()) && argList.get(0).equalsIgnoreCase("SETUP") || command.equalsIgnoreCase(CommandEnum.LICENSE.name()) )
			{
				hero = new Hero(context);
				hero.setNumber(number);
			}
			else
			{
				L.d(number + " tried to send a command without being a hero!");
				return;
			}
		}
		
		// License checking
		if( !command.equalsIgnoreCase(CommandEnum.HERO.name()) && !command.equalsIgnoreCase(CommandEnum.LOCATION.name()) && !command.equalsIgnoreCase(CommandEnum.LICENSE.name()) )
		{
			if( !LicenseUtils.getInstance(context).checkLicense())
			{
				hero.sendTextMessage( context.getString(R.string.license_fail, context.getString(R.string.site)) );
				return;
			}
		}
		
		executeCommand(context, command, hero, argList);
	}

	private boolean executeCommand(Context context, String command, Hero hero, ArrayList<String> argList)
	{
		try
		{
			return CommandManager.executeCommand(context, command, hero, argList);
		}
		catch (Exception e)
		{
			return false;
		}
	}
}
