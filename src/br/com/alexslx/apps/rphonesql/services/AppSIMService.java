package br.com.alexslx.apps.rphonesql.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import br.com.alexslx.apps.rphonesql.AppConfig;
import br.com.alexslx.apps.rphonesql.utils.L;
import br.com.alexslx.apps.rphonesql.utils.PhoneUtils;

public class AppSIMService extends Service
{
	// This is the object that receives interactions from clients
	private final IBinder	mBinder	= new LocalBinder();

	/**
	 * Class for clients to access. Because we know this service always runs in
	 * the same process as its clients, we don't need to deal with IPC.
	 */
	public class LocalBinder extends Binder
	{
		AppSIMService getService()
		{
			return AppSIMService.this;
		}
	}

	@Override
	public void onCreate()
	{
		L.d("SIM Checking Service Started!");
		new Thread(new SIMCheckingThread()).start();
	}

	@Override
	public void onDestroy()
	{
		L.d("SIM Checking Service has been stopped");
	}

	@Override
	public IBinder onBind(Intent intent)
	{
		return mBinder;
	}

	class SIMCheckingThread implements Runnable
	{

		public void run()
		{
			try
			{
				final TelephonyManager mTelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
				if (mTelephonyMgr.getSimState() != TelephonyManager.SIM_STATE_ABSENT)
				{
					while (mTelephonyMgr.getSimState() != TelephonyManager.SIM_STATE_READY)
					{
						Thread.sleep(5000);
					}
					
					String newSIMSerialNumber = "";
					if( mTelephonyMgr.getSimSerialNumber() != null )
						newSIMSerialNumber = mTelephonyMgr.getSimSerialNumber();
					
					if( !AppConfig.getInstance(getApplicationContext()).getSIMSerial().equalsIgnoreCase(newSIMSerialNumber) )
					{
						String oldSIMSerialNumber = AppConfig.getInstance(getApplicationContext()).getSIMSerial();
						PhoneUtils.onSIMChanged(getApplicationContext(), oldSIMSerialNumber, newSIMSerialNumber);
					}
				}
			}
			catch (Exception e)
			{
				L.d("Exception: " + e.getMessage());
			}
		}
	}
}
