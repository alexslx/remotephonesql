/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.rphonesql.services;

import java.util.List;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import br.com.alexslx.apps.rphonesql.AppConfig;
import br.com.alexslx.apps.rphonesql.models.AppLocation;
import br.com.alexslx.apps.rphonesql.models.LocationUpdate;
import br.com.alexslx.apps.rphonesql.utils.L;
import br.com.alexslx.apps.rphonesql.utils.NetworkUtils;

public class LocationService extends Service
{
	/**
	 * Last registered location by our providers.
	 */
	private Location		lastRegisteredLocation;

	/**
	 * This our location manager.
	 */
	private LocationManager	androidLocationManager;
	
	/**
	 * This service instance
	 */
	private static LocationService _instance;

	/**
	 * This is the object that receives commands from client.
	 */
	private final IBinder	mBinder	= new LocalBinder();
	
	/**
	 * Last time we've checked if this service is needed.
	 */
	private long last_check	= 0;

	/**
	 * Class for clients to access. Because we know this service always runs in
	 * the same process as its clients, we don't need to deal with IPC.
	 */
	public class LocalBinder extends Binder
	{
		LocationService getService()
		{
			return LocationService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent)
	{
		return mBinder;
	}
	
	public static boolean isRunning()
	{
		return ( LocationService._instance != null );
	}

	@Override
	public void onCreate()
	{
		LocationService._instance = this;
		
		AppConfig settings	= AppConfig.getInstance(LocationService.this);
		if( settings.getLocationCount() > 0 )
		{
			long updateInterval = settings.getLocationInterval();
			if( updateInterval < AppConfig.LOCATION_PROVIDERS_MIN_REFRESH_INTERVAL)
				updateInterval = AppConfig.LOCATION_PROVIDERS_MIN_REFRESH_INTERVAL;
			
			// Turn on wifi
			if (!NetworkUtils.getNetworkUtils(getApplicationContext()).isWifiEnabled())
				NetworkUtils.getNetworkUtils(getApplicationContext()).turnOnWifi(true);
			
			androidLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			LocationProvider gpsLocationProvider = androidLocationManager.getProvider(LocationManager.GPS_PROVIDER);
			LocationProvider networkProvider = androidLocationManager.getProvider(LocationManager.NETWORK_PROVIDER);
			
			// Enable GPS Provider if possible
			if (gpsLocationProvider != null && androidLocationManager.isProviderEnabled(gpsLocationProvider.getName()))
			{
				L.d("GPS Provider has been started.");
				androidLocationManager.requestLocationUpdates(gpsLocationProvider.getName(), updateInterval, AppConfig.LOCATION_PROVIDERS_MIN_REFRESH_DISTANCE, gpsLocationListener);
			}
			
			// Enable Network Provider if possible
			if (networkProvider != null && androidLocationManager.isProviderEnabled(networkProvider.getName()))
			{
				L.d("Network Provider has been started.");
				androidLocationManager.requestLocationUpdates(networkProvider.getName(), updateInterval / 4, AppConfig.LOCATION_PROVIDERS_MIN_REFRESH_DISTANCE, networkLocationListener);
			}
		}
	}

	@Override
	public void onDestroy()
	{
		if (androidLocationManager != null)
		{
			androidLocationManager.removeUpdates(gpsLocationListener);
			androidLocationManager.removeUpdates(networkLocationListener);
		}
		LocationService._instance = null;
	}
	
	/**
	 * Received a new location
	 * @param newLocation device location
	 */
	private void setNewLocation(Location newLocation)
	{
		L.d("[" + newLocation.getProvider() + "] Fix found! Accuracy: [" + newLocation.getAccuracy() + "]");

		if (lastRegisteredLocation == null)
		{
			L.d("First position fix.");
			lastRegisteredLocation = newLocation;
		}
		else
		{
			if( (newLocation.getTime() - lastRegisteredLocation.getTime())  > AppConfig.LAST_LOCATION_MAX_AGE)
			{
				L.d("Old position is outdated! Set this as last position fix!");
				lastRegisteredLocation = newLocation;
			}
			else if( newLocation.hasAccuracy() && (newLocation.getAccuracy() < lastRegisteredLocation.getAccuracy()) )
			{
				L.d("Newer and more accurate fix!");
				lastRegisteredLocation = newLocation;
			}
		}
		sendNewLocation(newLocation);
	}

	private void sendNewLocation(Location newLocation)
	{
		LocationUpdate lup = new LocationUpdate();
		lup.setLatitude(newLocation.getLatitude());
		lup.setLongitude(newLocation.getLongitude());
		lup.setAltitude(newLocation.getAltitude());
		lup.setAccuracy(newLocation.getAccuracy());
		lup.setSpeed(newLocation.getSpeed());
		
		List<AppLocation> locList = AppLocation.listAll(AppLocation.class);
		for(AppLocation loc : locList)
			loc.onUpdateLocation(LocationService.this, lup);
		
		AppConfig.getInstance(LocationService.this).setLastLocation(lup);
	}

	private LocationListener	gpsLocationListener		= new LocationListener()
	{
		public void onStatusChanged(String provider, int status, Bundle extras)
		{
			String statusAsString = "N/A";
			switch(status)
			{
				case LocationProvider.AVAILABLE:
					statusAsString = "AVAILABLE";
					break;
				case LocationProvider.OUT_OF_SERVICE:
					statusAsString = "OUT OF SERVICE";
					break;
				case LocationProvider.TEMPORARILY_UNAVAILABLE:
					statusAsString = "TEMPORARILY UNAVAILABLE";
					break;
				default:
					statusAsString = "Error";
					break;
			}
			
			L.d("[LocationService] GPS Location provider status has changed: [" + statusAsString + "].");
			checkIfNecessary();
		}

		public void onProviderEnabled(String provider)
		{
			L.d("[LocationService] GPS Location Provider has been enabled: " + provider);
		}

		public void onProviderDisabled(String provider)
		{
			L.d("[LocationService] GPS Location Provider has been disabled: " + provider);
		}

		public void onLocationChanged(Location location)
		{
			setNewLocation(location);
		}
	};

	private LocationListener	networkLocationListener	= new LocationListener()
	{
		public void onStatusChanged(String provider, int status, Bundle extras)
		{
			String statusAsString = "N/A";
			switch(status)
			{
				case LocationProvider.AVAILABLE:
					statusAsString = "AVAILABLE";
					break;
				case LocationProvider.OUT_OF_SERVICE:
					statusAsString = "OUT OF SERVICE";
					break;
				case LocationProvider.TEMPORARILY_UNAVAILABLE:
					statusAsString = "TEMPORARILY UNAVAILABLE";
					break;
				default:
					statusAsString = "Error";
					break;
			}
			
			L.d("[LocationService] Network Location provider status has changed: [" + statusAsString + "].");
			checkIfNecessary();
		}

		public void onProviderEnabled(String provider)
		{
			L.d("[LocationService] Network Location Provider has been enabled: " + provider);
		}

		public void onProviderDisabled(String provider)
		{
			L.d("[LocationService] Network Location Provider has been disabled: " + provider);
		}

		public void onLocationChanged(Location location)
		{
			setNewLocation(location);
		}
	};

	protected void checkIfNecessary()
	{
		long unixTime = System.currentTimeMillis() / 1000L;
		if( this.last_check == 0 )
			this.last_check = unixTime;
		
		long diff = unixTime - this.last_check;
		if( diff < AppConfig.LOCATION_SERVICE_CHECK_DELAY )
			return;
		
		List<AppLocation> locList = AppLocation.listAll(AppLocation.class);
		if( locList.size() == 0 )
			this.stopSelf();
	}

}
