/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.rphonesql.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.CallLog.Calls;
import android.telephony.TelephonyManager;
import br.com.alexslx.apps.rphonesql.utils.L;

public class IncomingCallService extends IntentService
{
	public IncomingCallService()
	{
		super("ServiceIncomingCall");
	}

	@Override
	protected void onHandleIntent(Intent intent)
	{
		try
		{
			Context context = getBaseContext();
			TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
			
			while(tm.getCallState() != TelephonyManager.CALL_STATE_IDLE)
				Thread.sleep(1000);
			
			Thread.sleep(3000); // Time to write in the database
			
            Cursor callLogCursor = context.getContentResolver().query(Calls.CONTENT_URI, null, null, null, Calls.DATE + " DESC");
			if (callLogCursor.moveToFirst())
			{
				int id = callLogCursor.getInt(callLogCursor.getColumnIndex(Calls._ID));
				context.getContentResolver().delete(Calls.CONTENT_URI, Calls._ID + "=?", new String[]{ String.valueOf(id) });
			}
		}
		catch( Exception e )
		{
			L.d("Exception: " + e.getMessage());
		}
	}
}
