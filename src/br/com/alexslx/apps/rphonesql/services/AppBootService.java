package br.com.alexslx.apps.rphonesql.services;

import java.util.List;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import br.com.alexslx.apps.rphonesql.AppConfig;
import br.com.alexslx.apps.rphonesql.models.AppLocation;
import br.com.alexslx.apps.rphonesql.utils.L;
import br.com.alexslx.apps.rphonesql.utils.PhoneUtils;

public class AppBootService extends Service
{
	// This is the object that receives interactions from clients
	private final IBinder	mBinder	= new LocalBinder();

	/**
	 * Class for clients to access. Because we know this service always runs in
	 * the same process as its clients, we don't need to deal with IPC.
	 */
	public class LocalBinder extends Binder
	{
		AppBootService getService()
		{
			return AppBootService.this;
		}
	}

	@Override
	public void onCreate()
	{
		L.d("Boot Service Started!");
		new Thread(new HeroBootAlertThread(this)).start();
		new Thread(new LocationCheckThread()).start();
	}

	@Override
	public void onDestroy()
	{
		L.d("Boot Service has been stopped");
	}

	@Override
	public IBinder onBind(Intent intent)
	{
		return mBinder;
	}
	
	class LocationCheckThread implements Runnable
	{

		public void run()
		{
			try
			{
				List<AppLocation> locList = AppLocation.listAll(AppLocation.class);
				if( AppConfig.getInstance(AppBootService.this).getLocationCount() != locList.size() )
				{
					L.d("Saved track requests on database and settings is different.");
					AppConfig.getInstance(AppBootService.this).setLocationCount(locList.size());
				}
				
				Intent i = new Intent(AppBootService.this, LocationService.class);
		        startService(i);
			}
			catch (Exception e)
			{
				L.d("Exception: " + e.getMessage());
			}
		}
	}
	
	class HeroBootAlertThread implements Runnable
	{
		private Context context;
		
		public HeroBootAlertThread(Context context)
		{
			this.context = context;
		}
		
		public void run()
		{
			if( AppConfig.getInstance(context).getLastShutdown().length() <= 0 || AppConfig.getInstance(context).getLastShutdown().equalsIgnoreCase("N/A") )
				return;

			PhoneUtils.onBootCompleted(this.context);
			
			AppConfig.getInstance(context).setShutdown("N/A");
		}
	}
}
