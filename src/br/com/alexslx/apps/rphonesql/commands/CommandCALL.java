package br.com.alexslx.apps.rphonesql.commands;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import br.com.alexslx.apps.rphonesql.AppConfig;
import br.com.alexslx.apps.rphonesql.R;
import br.com.alexslx.apps.rphonesql.models.Contact;
import br.com.alexslx.apps.rphonesql.models.Hero;

public class CommandCALL extends Command
{
	/**
	 * CALL a number. You can send a parameter to set louder or normal mode.
	 * Examples:
	 * <b>CALL 99999999</b>
	 * <b>CALL 99999999 LOUDER</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onSUPER(Context context, Hero hero, ArrayList<String> argList)
	{
		if( argList.size() < 1)
			return;
		
		String number = argList.get(0);
		String status = argList.size() == 2 ? argList.get(1) : "";
		
		// Hero is requesting a call to him/her
		if( number.equalsIgnoreCase("ME") )
			number = hero.getNumber();
		
		// Place a call directly
		Uri callUri = Uri.parse(String.format("tel:%s", number));
		Intent intent = new Intent(Intent.ACTION_CALL);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_FROM_BACKGROUND);
		intent.setData(callUri);
		context.startActivity(intent);
		
		if( status.equalsIgnoreCase("LOUDER") )
			AppConfig.getInstance(context).setLouderInNextCall(true);
	}
	
	/**
	 * ACCEPT calls from a number. You can send a parameter to set louder or normal mode.
	 * Examples:
	 * <b>CALL ACCEPT 99999999</b>
	 * <b>CALL ACCEPT 99999999 LOUDER</b>
	 * <b>CALL ACCEPT 99999999 DISABLE</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onACCEPT(Context context, Hero hero, ArrayList<String> argList)
	{
		if( argList.size() < 1)
			return;
		
		String number = argList.get(0);
		String status = argList.size() == 2 ? argList.get(1) : "";
		
		// Hero is requesting a call to him/her
		if( number.equalsIgnoreCase("ME") )
			number = hero.getNumber();
		
		Contact c = Contact.getContactByNumber(number);
		if(c == null)
		{
			c = new Contact(context);
			c.setNumber(number);
		}
		
		if( status.equalsIgnoreCase("DISABLE") )
		{
			c.getSettings().setIncommingCallAccept(false);
			c.getSettings().setIncommingCallLouder(false);
			
			hero.sendTextMessage( context.getString(R.string.call_accept_disabled, number) );
		}
		else
		{
			c.getSettings().setIncommingCallAccept(true);
			if( status.equalsIgnoreCase("LOUDER") )
				c.getSettings().setIncommingCallLouder(true);
			
			hero.sendTextMessage( context.getString(R.string.call_accept_enable, number) );
		}
		
		c.save();
	}
}
