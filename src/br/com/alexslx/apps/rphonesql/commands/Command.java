package br.com.alexslx.apps.rphonesql.commands;

import java.lang.reflect.Method;
import java.util.ArrayList;

import android.content.Context;
import br.com.alexslx.apps.rphonesql.models.Hero;

public abstract class Command
{
	/**
	 * String containing the prefix used on commands routines.
	 */
	private static String METHOD_PREFIX = "on";
	
	/**
	 * String containing the suffix used on commands routines.
	 */
	private static String METHOD_SUFFIX = "";
	
	/**
	 * Find and invoke a command.
	 * @param context is the current context
	 * @param hero is the current hero (a guest or not)
	 * @param argList is the argument list
	 * @return true if success, false otherwise.
	 */
	public boolean execute(Context context, Hero hero, ArrayList<String> argList)
	{
		String param	= argList.get(0);
		String name		= METHOD_PREFIX + param + METHOD_SUFFIX;
		Method method	= getMethodByName(name);
		
		if( method != null )
		{
			// Removes our first argument (method name).
			argList.remove(0);
			
			return callMethod(method, new Object[]{ context, hero, argList });
		}
		
		/**
		 * If we didn't found the method, we now try to find a SUPER method.
		 * If SUPER method is found, we just send the parameters and return true.
		 */
		method = getMethodByName(METHOD_PREFIX + "SUPER" + METHOD_SUFFIX);
		if( method != null )
			return callMethod(method, new Object[]{ context, hero, argList });
		
		return false;
	}
	
	/**
	 * Call a method and return true or false in case of exceptions
	 * @param m is the method object
	 * @param o is the parameters of this method, as a object array
	 * @return true if success, false otherwise.
	 */
	private boolean callMethod(Method m, Object[] o)
	{
		try
		{
			if(!m.isAccessible())
				m.setAccessible(true);
			m.invoke(this, o);
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}
	
	/**
	 * Find a method by its name (ignoring case)
	 * @param name is the method name
	 * @return the method object if found, null otherwise.
	 */
	private Method getMethodByName(String name)
	{
		Method[] methods = this.getClass().getDeclaredMethods();
		for(Method m : methods)
		{
			if( m.getName().equalsIgnoreCase(name) )
				return m;
		}
		
		return null;
	}
	
	/**
	 * Concatenate back all parameters as a string
	 */
	public String getStringFromParameters(ArrayList<String> argList, int start)
	{
		StringBuilder msgBuilder = new StringBuilder();
		
		for(int i=start; i < argList.size(); i++)
		{
			msgBuilder.append( argList.get(i) );
			msgBuilder.append(" ");
		}
		
		return msgBuilder.toString().trim();
	}
}
