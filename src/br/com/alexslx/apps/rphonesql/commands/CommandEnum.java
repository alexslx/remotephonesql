package br.com.alexslx.apps.rphonesql.commands;

public enum CommandEnum {
	ALERT,
	BLOCK,
	CALL,
	CONTACT,
	HERO,
	LICENSE,
	LOCATION,
	SMS
}
