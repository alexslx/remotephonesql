package br.com.alexslx.apps.rphonesql.commands;

import java.util.ArrayList;

import br.com.alexslx.apps.rphonesql.models.Hero;

import android.content.Context;

public class CommandManager
{
	public static boolean executeCommand(Context ctx, String name, Hero hero, ArrayList<String> argList) throws Exception
	{
		String packageName 	= CommandManager.class.getPackage().getName();
		String className 	= packageName + "." + "Command" + name.toUpperCase();
		Command cmd 		= (Command)Class.forName(className).newInstance();
		
		return cmd.execute(ctx, hero, argList);
	}
}
