package br.com.alexslx.apps.rphonesql.commands;

import java.util.ArrayList;

import android.content.Context;
import br.com.alexslx.apps.alc.LicenseUtils;
import br.com.alexslx.apps.rphonesql.R;
import br.com.alexslx.apps.rphonesql.models.Hero;

public class CommandLICENSE extends Command
{
	/**
	 * ACTIVATE our application trough alternative license checking.
	 * Examples:
	 * <b>LICENSE ACTIVATE ###########</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onACTIVATE(Context context, Hero hero, ArrayList<String> argList)
	{
		if( argList.size() < 1)
			return;
		
		String activation = argList.get(0);
		
		if( LicenseUtils.getInstance(context).licenseActivate(activation) )
			hero.sendTextMessage( context.getString(R.string.license_success) );
		else
			hero.sendTextMessage( context.getString(R.string.license_fail, context.getString(R.string.site) ) );
	}
	
	/**
	 * RESET our application trough alternative license checking.
	 * Examples:
	 * <b>LICENSE RESET</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onRESET(Context context, Hero hero, ArrayList<String> argList)
	{
		LicenseUtils.getInstance(context).resetLicense();
		
		hero.sendTextMessage(context.getString(R.string.license_reseted));
	}
}
