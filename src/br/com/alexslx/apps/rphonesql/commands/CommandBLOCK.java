package br.com.alexslx.apps.rphonesql.commands;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import br.com.alexslx.apps.rphonesql.AppConfig;
import br.com.alexslx.apps.rphonesql.R;
import br.com.alexslx.apps.rphonesql.models.Contact;
import br.com.alexslx.apps.rphonesql.models.Hero;

public class CommandBLOCK extends Command
{
	/**
	 * BLOCK ALL of a person or any person.
	 * Examples:
	 * <b>BLOCK ALL</b>
	 * <b>BLOCK ALL DISABLE</b>
	 * <b>BLOCK ALL 99999999</b> - Disabled because Android doesn't allow us to intercept outgoing SMS.
	 * <b>BLOCK ALL 99999999 DISABLE</b> - Disabled because Android doesn't allow us to intercept outgoing SMS.
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onALL(Context context, Hero hero, ArrayList<String> argList)
	{
		String arg1		= argList.size() > 0 ? argList.get(0) : "";
		String arg2 	= argList.size() > 1 ? argList.get(1) : "";
		boolean check 	= true;
		
		if(arg1.equalsIgnoreCase("DISABLE"))
			check = false;
		
		if(!arg1.isEmpty() && arg2.equalsIgnoreCase("DISABLE"))
			check = false;
		
		
		/**
		 * Cover this examples: <b>BLOCK ALL $NUMBER</b> or <b>BLOCK ALL $NUMBER DISABLE</b>
		 * TODO: Android doesn't allow us to intercept outgoing SMS.
		 */
		/*if( !arg1.isEmpty() && !arg1.equalsIgnoreCase("DISABLE") )
		{
			Contact c = Contact.getContactByNumber(arg1);
			if(c == null)
			{
				c = new Contact(context);
				c.setNumber(arg1);
			}
			
			c.getSettings().setBlockAll(check);
			c.save();
			
			if(check)
				hero.sendTextMessage( context.getString(R.string.block_all_nr_enabled, arg1) );
			else
				hero.sendTextMessage( context.getString(R.string.block_all_nr_disabled, arg1) );
		}*/
		
		if( arg1.isEmpty() || arg1.equalsIgnoreCase("DISABLE") )
		{
			AppConfig.getInstance(context).setBlocked("ALL", check);
			
			if(check)
				hero.sendTextMessage( context.getString(R.string.block_all_enabled) );
			else
				hero.sendTextMessage( context.getString(R.string.block_all_disabled) );
		}
	}
	
	/**
	 * BLOCK SMS of a person or any person.
	 * Examples:
	 * <b>BLOCK SMS ALL</b>
	 * <b>BLOCK SMS ALL DISABLE</b>
	 * <b>BLOCK SMS ALL 99999999</b>
	 * <b>BLOCK SMS ALL 99999999 DISABLE</b>
	 * <b>BLOCK SMS FROM 99999999</b>
	 * <b>BLOCK SMS FROM 99999999 DISABLE</b>
	 * <b>BLOCK SMS TO 99999999</b> - Disabled because Android doesn't allow us to intercept outgoing SMS.
	 * <b>BLOCK SMS TO 99999999 DISABLE</b> - Disabled because Android doesn't allow us to intercept outgoing SMS.
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onSMS(Context context, Hero hero, ArrayList<String> argList)
	{
		if( argList.size() < 1)
			return;
		
		String type 	= argList.get(0);
		String arg1 	= argList.size() > 1 ? argList.get(1) : "";
		String arg2 	= argList.size() > 2 ? argList.get(2) : "";
		boolean check 	= true;
		
		if( type.equalsIgnoreCase("ALL") )
		{
			if(arg1.isEmpty())
			{
				AppConfig.getInstance(context).setBlocked("SMS", true);
				
				hero.sendTextMessage( context.getString(R.string.block_sms_all_enabled) );
			}
			else if(arg1.equalsIgnoreCase("DISABLE"))
			{
				AppConfig.getInstance(context).setBlocked("SMS", false);
				
				hero.sendTextMessage( context.getString(R.string.block_sms_all_disabled) );
			}
			else
			{
				Contact c = Contact.getContactByNumber(arg1);
				if(c == null)
				{
					c = new Contact(context);
					c.setNumber(arg1);
				}
				
				if(arg2.equalsIgnoreCase("DISABLE"))
					check = false;
				
				c.getSettings().setIncommingMsgBlock(check);
				c.getSettings().setOutgoingMsgBlock(check);
				c.save();
				
				if(check)
					hero.sendTextMessage( context.getString(R.string.block_sms_all_nr_enabled, arg1) );
				else
					hero.sendTextMessage( context.getString(R.string.block_sms_all_nr_disabled, arg1) );
			}
		}
		else if( type.equalsIgnoreCase("TO") )
		{
			boolean unavaliable = true;
			if(unavaliable)
			{
				hero.sendTextMessage( context.getString(R.string.block_sms_to_unavailable) );
			}
			else
			{
				Contact c = Contact.getContactByNumber(arg1);
				if(c == null)
				{
					c = new Contact(context);
					c.setNumber(arg1);
				}
				
				if(arg2.equalsIgnoreCase("DISABLE"))
					check = false;
				
				c.getSettings().setOutgoingMsgBlock(check);
				c.save();
				
				if(check)
					hero.sendTextMessage( context.getString(R.string.block_sms_to_enabled, arg1) );
				else
					hero.sendTextMessage( context.getString(R.string.block_sms_to_disabled, arg1) );
			}
		}
		else if( type.equalsIgnoreCase("FROM") )
		{
			Contact c = Contact.getContactByNumber(arg1);
			if(c == null)
			{
				c = new Contact(context);
				c.setNumber(arg1);
			}
			
			if(arg2.equalsIgnoreCase("DISABLE"))
				check = false;
			
			c.getSettings().setIncommingMsgBlock(check);
			c.save();
			
			if(check)
				hero.sendTextMessage( context.getString(R.string.block_sms_from_enabled, arg1) );
			else
				hero.sendTextMessage( context.getString(R.string.block_sms_from_disabled, arg1) );
		}
	}
	
	/**
	 * BLOCK CALL of a person or any person.
	 * Examples:
	 * <b>BLOCK CALL ALL</b>
	 * <b>BLOCK CALL ALL DISABLE</b>
	 * <b>BLOCK CALL ALL 99999999</b>
	 * <b>BLOCK CALL ALL 99999999 DISABLE</b>
	 * <b>BLOCK CALL FROM 99999999</b>
	 * <b>BLOCK CALL FROM 99999999 DISABLE</b>
	 * <b>BLOCK CALL TO 99999999</b>
	 * <b>BLOCK CALL TO 99999999 DISABLE</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onCALL(Context context, Hero hero, ArrayList<String> argList)
	{
		if( argList.size() < 1)
			return;
		
		String type 	= argList.get(0);
		String arg1 	= argList.size() > 1 ? argList.get(1) : "";
		String arg2 	= argList.size() > 2 ? argList.get(2) : "";
		boolean check 	= true;
		
		if( type.equalsIgnoreCase("ALL") )
		{
			if(arg1.isEmpty())
			{
				AppConfig.getInstance(context).setBlocked("CALL", true);
				
				hero.sendTextMessage( context.getString(R.string.block_call_all_enabled) );
			}
			else if(arg1.equalsIgnoreCase("DISABLE"))
			{
				AppConfig.getInstance(context).setBlocked("CALL", false);
				
				hero.sendTextMessage( context.getString(R.string.block_call_all_disabled) );
			}
			else
			{
				Contact c = Contact.getContactByNumber(arg1);
				if(c == null)
				{
					c = new Contact(context);
					c.setNumber(arg1);
				}
				
				if(arg2.equalsIgnoreCase("DISABLE"))
					check = false;
				
				c.getSettings().setIncommingCallBlock(check);
				c.getSettings().setOutgoingCallBlock(check);
				c.save();
				
				if(check)
					hero.sendTextMessage( context.getString(R.string.block_call_all_nr_enabled, arg1) );
				else
					hero.sendTextMessage( context.getString(R.string.block_call_all_nr_disabled, arg1) );
			}
		}
		else if( type.equalsIgnoreCase("TO") )
		{
			Contact c = Contact.getContactByNumber(arg1);
			if(c == null)
			{
				c = new Contact(context);
				c.setNumber(arg1);
			}
			
			if(arg2.equalsIgnoreCase("DISABLE"))
				check = false;
			
			c.getSettings().setOutgoingCallBlock(check);
			c.save();
			
			if(check)
				hero.sendTextMessage( context.getString(R.string.block_call_to_enabled, arg1) );
			else
				hero.sendTextMessage( context.getString(R.string.block_call_to_disabled, arg1) );
		}
		else if( type.equalsIgnoreCase("FROM") )
		{
			Contact c = Contact.getContactByNumber(arg1);
			if(c == null)
			{
				c = new Contact(context);
				c.setNumber(arg1);
			}
			
			if(arg2.equalsIgnoreCase("DISABLE"))
				check = false;
			
			c.getSettings().setIncommingCallBlock(check);
			c.save();
			
			if(check)
				hero.sendTextMessage( context.getString(R.string.block_call_from_enabled, arg1) );
			else
				hero.sendTextMessage( context.getString(R.string.block_call_from_disabled, arg1) );
		}
	}
	
	/**
	 * LIST contacts with restrictions
	 * Example: <b>BLOCK LIST</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onLIST(Context context, Hero hero, ArrayList<String> argList)
	{
		AppConfig cfg = AppConfig.getInstance(context);
		
		/**
		 * Global Blocks
		 */
		String message	= context.getString(R.string.block_list_global);
		if( cfg.isBlocked("SMS") && cfg.isBlocked("CALL") )
			message += context.getString(R.string.block_list_sms) + "," + context.getString(R.string.block_list_calls);
		else if( cfg.isBlocked("SMS") )
			message += context.getString(R.string.block_list_sms);
		else if( cfg.isBlocked("CALL") )
			message += context.getString(R.string.block_list_calls);
		
		message += "\n";
		
		List<Contact> contactList = Contact.listAll(Contact.class);
		for(Contact c : contactList)
		{
			message += c.getNumber() + ": ";
			
			// Calls
			if( c.getSettings().isIncommingCallBlocked() && c.getSettings().isOutgoingCallBlocked() )
				message += String.format("%s (%s)", context.getString(R.string.block_list_calls), context.getString(R.string.block_list_all));
			else if( c.getSettings().isIncommingCallBlocked() )
				message += String.format("%s (%s)", context.getString(R.string.block_list_calls), context.getString(R.string.block_list_in));
			else if( c.getSettings().isOutgoingCallBlocked() )
				message += String.format("%s (%s)", context.getString(R.string.block_list_calls), context.getString(R.string.block_list_out));
			
			if(!message.endsWith(": "))
				message += ", ";
			
			// Messages
			if( c.getSettings().isIncommingMsgBlocked() && c.getSettings().isOutgoingMsgBlocked() )
				message += String.format("%s (%s)", context.getString(R.string.block_list_sms), context.getString(R.string.block_list_all));
			else if( c.getSettings().isIncommingMsgBlocked() )
				message += String.format("%s (%s)", context.getString(R.string.block_list_sms), context.getString(R.string.block_list_in));
			else if( c.getSettings().isOutgoingMsgBlocked() )
				message += String.format("%s (%s)", context.getString(R.string.block_list_sms), context.getString(R.string.block_list_out));
			
			message += "\n";
		}
		
		hero.sendTextMessage(message);
	}
}
