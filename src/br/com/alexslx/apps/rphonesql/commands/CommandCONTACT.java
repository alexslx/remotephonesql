package br.com.alexslx.apps.rphonesql.commands;

import java.util.ArrayList;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import br.com.alexslx.apps.rphonesql.R;
import br.com.alexslx.apps.rphonesql.models.Hero;

public class CommandCONTACT extends Command
{
	/**
	 * GET a list of contacts by a part of string.
	 * Example: <b>CONTACT GET Juca
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onGET(Context context, Hero hero, ArrayList<String> argList)
	{
		String name = getStringFromParameters(argList, 0);
		if(name.length() == 0)
			return;
		
		ContentResolver contentResolver	= context.getContentResolver();
		String[] projection				= { ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts._ID, ContactsContract.Contacts.HAS_PHONE_NUMBER };
		String selection				= String.format("%s LIKE '%%%s%%'", ContactsContract.Contacts.DISPLAY_NAME, name);
		String sortOrder				= "UPPER(" + ContactsContract.Contacts.DISPLAY_NAME + ") ASC";
		StringBuilder message			= new StringBuilder();
		
		try {
			Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, projection, selection, null, sortOrder);
			while (cursor.moveToNext())
			{
				String contactId	= cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
				String displayName	= cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
				String hasPhone		= cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
				Cursor dataCursor	= null;
				if (hasPhone.equalsIgnoreCase("0"))
					continue;
				
				message.append(displayName + ":\n");
				
				dataCursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
				while (dataCursor.moveToNext())
				{
					String number  = dataCursor.getString( dataCursor.getColumnIndex(ContactsContract.Data.DATA1) );
					message.append("* " + android.telephony.PhoneNumberUtils.stripSeparators(number) + "\n");
				}
				dataCursor.close();
			}
		}
		catch(Exception e){}
		
		if( message.length() == 0 )
			hero.sendTextMessage( context.getString(R.string.contact_notfound, name) );
		else
			hero.sendTextMessage( message.toString() );
	}
	
	/**
	 * DELETE a contact from device.
	 * Example: <b>CONTACT DELETE Juca Santos</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onDELETE(Context context, Hero hero, ArrayList<String> argList)
	{
		String name = getStringFromParameters(argList, 0);
		
		ContentResolver contentResolver	= context.getContentResolver();
		String[] projection				= { ContactsContract.Contacts.LOOKUP_KEY, ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts._ID };
		String selection				= String.format("%s LIKE '%s'", ContactsContract.Contacts.DISPLAY_NAME, name);
		
		try {
			Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, projection, selection, null, null);
			while (cursor.moveToNext())
			{
				String lookupKey = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY));
				String displayName	= cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
				
		        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey);
		        contentResolver.delete(uri, null, null);
		        
				hero.sendTextMessage( context.getString(R.string.contact_deleted, displayName) );
				return;
			}
		}
		catch(Exception e)
		{
			//L.d("Error: " + e.getMessage());
		}
		
		hero.sendTextMessage( context.getString(R.string.contact_notfound, name) );
	}
	
}
