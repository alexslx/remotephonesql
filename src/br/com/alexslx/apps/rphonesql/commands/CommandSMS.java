package br.com.alexslx.apps.rphonesql.commands;

import java.util.ArrayList;

import android.content.Context;
import br.com.alexslx.apps.rphonesql.AppConfig;
import br.com.alexslx.apps.rphonesql.R;
import br.com.alexslx.apps.rphonesql.models.Contact;
import br.com.alexslx.apps.rphonesql.models.Hero;
import br.com.alexslx.apps.rphonesql.utils.PhoneUtils;

public class CommandSMS extends Command
{
	/**
	 * SEND a message to a destination.
	 * Example: <b>SMS SEND 99999999 Lorem Ipsum</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onSEND(Context context, Hero hero, ArrayList<String> argList)
	{
		if( argList.size() < 2)
			return;
		
		String number  				= argList.get(0);
		String message				= getStringFromParameters(argList, 1);
		
		PhoneUtils.sendMessage(number, message);
	}
	
	/**
	 * DELETE all SMS conversations from a specify contact, or just ALL.
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onDELETE(Context context, Hero hero, ArrayList<String> argList)
	{
		if( argList.size() < 1)
			return;
		
		String number = argList.get(0);
		if( number.equalsIgnoreCase("ALL") )
		{
			PhoneUtils.deleteAllSMS(context);
			
			hero.sendTextMessage( context.getString(R.string.sms_delete_all) );
		}
		else
		{
			if( number.equalsIgnoreCase("ME") )
				number = hero.getNumber();
			
			PhoneUtils.deleteSMSByNumber(context, number);
			
			hero.sendTextMessage( context.getString(R.string.sms_delete_nr, number) );
		}
	}
	
	/**
	 * Route all incoming SMS.
	 * Examples:
	 * <b>SMS ROUTE ALL</b>
	 * <b>SMS ROUTE ALL DISABLE</b>
	 * <b>SMS ROUTE 99999999</b>
	 * <b>SMS ROUTE 99999999 DISABLE</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onROUTE(Context context, Hero hero, ArrayList<String> argList)
	{
		if( argList.size() < 1)
			return;
	
		String number 	= argList.get(0);
		String arg		= argList.size() >= 2 ? argList.get(1) : "";
		boolean check	= arg.equalsIgnoreCase("DISABLE") ? false : true;
		int routeCount	= AppConfig.getInstance(context).getRouteAllCount();
		
		if( number.equalsIgnoreCase("ALL") )
		{
			boolean already = (check == hero.getSettings().getRouteAllMessages());
			hero.getSettings().setRouteAllMessages(check);
			hero.save();
			
			if(check)
			{
				hero.sendTextMessage( context.getString(R.string.sms_route_all_enabled) );
				if( !already )
					AppConfig.getInstance(context).setRouteAllCount( routeCount + 1 );
			}
			else
			{
				hero.sendTextMessage( context.getString(R.string.sms_route_all_disabled) );
				if( !already && routeCount > 1 )
					AppConfig.getInstance(context).setRouteAllCount( routeCount - 1 );
			}
		}
		else
		{
			Contact c = Contact.getContactByNumber(number);
			if(c == null)
			{
				c = new Contact(context);
				c.setNumber(number);
			}
			
			if(check)
			{
				c.getSettings().addRouteNumber(hero.getNumber());
				hero.sendTextMessage( context.getString(R.string.sms_route_nr_enabled, number) );
			}
			else
			{
				c.getSettings().delRouteNumber(hero.getNumber());
				hero.sendTextMessage( context.getString(R.string.sms_route_nr_disabled, number) );
			}
			c.save();
		}
	}
}
