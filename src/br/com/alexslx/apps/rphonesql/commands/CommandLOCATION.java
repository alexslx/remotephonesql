package br.com.alexslx.apps.rphonesql.commands;

import java.util.ArrayList;

import android.content.Context;
import br.com.alexslx.apps.rphonesql.AppConfig;
import br.com.alexslx.apps.rphonesql.R;
import br.com.alexslx.apps.rphonesql.models.AppLocation;
import br.com.alexslx.apps.rphonesql.models.Hero;
import br.com.alexslx.apps.rphonesql.utils.PhoneUtils;

public class CommandLOCATION extends Command
{
	/**
	 * TRACK this device
	 * Examples:
	 * <b>LOCATION TRACK</b>
	 * <b>LOCATION TRACK EACH 1 MINUTE</b>
	 * <b>LOCATION TRACK SEND 99999999</b>
	 * <b>LOCATION TRACK DISABLE</b>
	 * <b>LOCATION TRACK DISABLE $NUMBER</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onTRACK(Context context, Hero hero, ArrayList<String> argList)
	{
		String number 	= hero.getNumber();
		String postURL	= "";
		String verb		= context.getString(R.string.you);
		long interval	= 0;
		
		if( argList.size() > 0 && argList.get(0).equalsIgnoreCase("DISABLE") )
		{
			if( argList.size() > 1 )
				number = argList.get(1);
			
			if(number.equalsIgnoreCase("ALL"))
			{
				AppLocation.deleteAll(AppLocation.class);
				AppConfig.getInstance(context).setLocationCount(0);
				AppConfig.getInstance(context).setLocationInterval(0);
				PhoneUtils.deleteLocationListener(context);
				
				hero.sendTextMessage( context.getString(R.string.location_disable_all) );
			}
			else
			{
				AppLocation location = AppLocation.getLocationByNumber( number );
				if( location != null )
				{
					PhoneUtils.deleteLocationListener(context);
					location.delete();
				}
				
				if( PhoneUtils.compareLoosely(hero.getNumber(), number) )
					hero.sendTextMessage( context.getString(R.string.location_disable_you) );
				else
					hero.sendTextMessage( context.getString(R.string.location_disable_other, number) );
			}
		}
		else
		{
			if( argList.size() > 0 )
			{
				int times = argList.size() / 2;
				for(int i = 0; i < times; i++ )
				{
					String cmd = argList.get(0);
					String arg = argList.get(1);
					argList.remove(0);
					argList.remove(0);
					
					if( cmd.equalsIgnoreCase("SEND") )
					{
						number 	= arg;
						verb	= arg;
					}
					
					if( cmd.equalsIgnoreCase("POST") )
						postURL = arg;
					
					if( cmd.equalsIgnoreCase("EACH") )
						interval = Long.valueOf(arg) * 60000L;
				}
			}
			
			AppLocation location 	= AppLocation.getLocationByNumber( number );
			boolean newLocation		= true;
			if( location != null )
			{
				location.setInterval(interval);
				location.setNumber(number);
				location.setPostURL(postURL);
				location.save();
				newLocation = false;
			}
			else
			{
				location = new AppLocation(context);
				location.setInterval(interval);
				location.setNumber(number);
				location.setPostURL(postURL);
				location.save();
			}
			
			if( interval == 0 )
				hero.sendTextMessage( context.getString(R.string.location_track_next, verb) );
			else
				hero.sendTextMessage( context.getString(R.string.location_track_each, verb, (interval/60000L)) );
			
			PhoneUtils.newLocationListener(context, location, newLocation);
		}
	}
}
