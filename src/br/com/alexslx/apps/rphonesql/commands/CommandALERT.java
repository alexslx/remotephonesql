package br.com.alexslx.apps.rphonesql.commands;

import java.util.ArrayList;

import android.content.Context;
import br.com.alexslx.apps.rphonesql.R;
import br.com.alexslx.apps.rphonesql.models.Hero;

public class CommandALERT extends Command
{
	/**
	 * BOOT received a message when device boots up.
	 * Examples:
	 * <b>ALERT BOOT</b>
	 * <b>ALERT BOOT DISABLE</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onBOOT(Context context, Hero hero, ArrayList<String> argList)
	{
		boolean check			= true;
		
		if( argList.size() > 0 && argList.get(0).equalsIgnoreCase("DISABLE") )
			check = false;
		
		hero.getSettings().setAlertOnBoot(check);
		hero.save();
		
		if (check)
			hero.sendTextMessage(context.getString(R.string.alert_boot_enabled));
		else
			hero.sendTextMessage(context.getString(R.string.alert_boot_disabled));
	}
	
	/**
	 * SIM received a message when device change its SIM.
	 * Examples:
	 * <b>ALERT SIM</b>
	 * <b>ALERT SIM DISABLE</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onSIM(Context context, Hero hero, ArrayList<String> argList)
	{
		boolean check			= true;
		
		if( argList.size() > 0 && argList.get(0).equalsIgnoreCase("DISABLE") )
			check = false;
		
		hero.getSettings().setAlertOnSIMChange(check);
		hero.save();
		
		if (check)
			hero.sendTextMessage(context.getString(R.string.alert_sim_enabled));
		else
			hero.sendTextMessage(context.getString(R.string.alert_sim_disabled));
	}
}
