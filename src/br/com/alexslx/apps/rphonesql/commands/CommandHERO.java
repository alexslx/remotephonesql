package br.com.alexslx.apps.rphonesql.commands;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import br.com.alexslx.apps.rphonesql.AppConfig;
import br.com.alexslx.apps.rphonesql.R;
import br.com.alexslx.apps.rphonesql.models.Hero;
import br.com.alexslx.apps.rphonesql.utils.L;

public class CommandHERO extends Command
{
	/**
	 * SETUP a new hero. If here is the first hero, he'll need to setup a password, else he'll need to send the correct password.
	 * Example: <b>HERO SETUP 123456</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onSETUP(Context context, Hero hero, ArrayList<String> argList)
	{
		if( argList.size() < 1)
			return;
		
		List<Hero> heroList 	= Hero.listAll(Hero.class);
		String password 		= argList.get(0).toUpperCase();
		
		if(heroList.size() == 0)
		{
			AppConfig.getInstance(context).setHeroPassword(password);
			
			hero.save();
			hero.sendTextMessage( context.getString(R.string.hero_setup_first) );
		}
		else
		{
			if( password.compareToIgnoreCase( AppConfig.getInstance(context).getHeroPassword() ) == 0 )
			{
				Hero h = Hero.getHeroByNumber(hero.getNumber());
				if(h != null)
				{
					hero.sendTextMessage( context.getString(R.string.hero_setup_already) );
				}
				else
				{
					hero.save();
					hero.sendTextMessage( context.getString(R.string.hero_setup_success) );
				}
			}
			else
			{
				L.d(hero.getNumber() + " tried a wrong password!");
			}
		}
	}
	
	/**
	 * LIST all current heroes registered.
	 * Example: <b>HERO LIST</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onLIST(Context context, Hero hero, ArrayList<String> argList)
	{
		List<Hero> heroList = Hero.listAll(Hero.class);
		
		if(heroList.size() < 2)
		{
			hero.sendTextMessage( context.getString(R.string.hero_list_unique) );
			return;
		}
		
		StringBuilder strBuilder = new StringBuilder( context.getString(R.string.hero_list_header) );
		for(Hero h : heroList)
			strBuilder.append( h.getNumber() ).append("\n");
		
		hero.sendTextMessage(strBuilder.toString());
	}
	
	/**
	 * ADD a hero to current heroes list.
	 * Example: <b>HERO ADD 99999999</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onADD(Context context, Hero hero, ArrayList<String> argList)
	{
		if( argList.size() < 1)
			return;
		
		String number = argList.get(0);
		Hero h = new Hero(context);
		h.setNumber(number);
		h.save();
		
		hero.sendTextMessage( context.getString(R.string.hero_add_success, number) );
	}

	/**
	 * DELETE a hero from current heroes list. You can replace the hero number by word ME or ALL. ALL command doesn't remove yourself. 
	 * Examples:
	 * <b>HERO REMOVE 123456</b>
	 * <b>HERO REMOVE ME</b>
	 * <b>HERO REMOVE ALL</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onDELETE(Context context, Hero hero, ArrayList<String> argList)
	{
		if( argList.size() < 1)
			return;
		
		String number = argList.get(0);
		if( number.equalsIgnoreCase("ALL") )
		{
			Hero.deleteAll(Hero.class);			
			hero.sendTextMessage( context.getString(R.string.hero_del_all) );
			
			/**
			 * Setup a new hero and save it to database.
			 */
			Hero h = new Hero(context);
			h.setNumber( hero.getNumber() );
			h.save();
		}
		else 
		{
			boolean autoRemove = false;
			if(number.equalsIgnoreCase("ME"))
			{
				autoRemove = true;
				number = hero.getNumber();
			}
			
			Hero h = Hero.getHeroByNumber(number);
			if(h == null)
			{
				hero.sendTextMessage( context.getString(R.string.hero_del_notfound, number) );
				return;
			}
			
			h.delete();
			
			if(autoRemove)
				hero.sendTextMessage( context.getString(R.string.hero_del_itself) );
			else
				hero.sendTextMessage( context.getString(R.string.hero_del_success, number) );
		}
	}
	
	/**
	 * Change the PASSWORD to setup a new hero.
	 * Example: <b>HERO PASSWORD 123456</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onPASSWORD(Context context, Hero hero, ArrayList<String> argList)
	{
		if( argList.size() < 1)
			return;
		
		String newpass = argList.get(0);
		List<Hero> heroList = Hero.listAll(Hero.class);
		
		for(Hero h : heroList)
			h.sendTextMessage( context.getString(R.string.hero_pass_change, newpass) );
		
		AppConfig.getInstance(context).setHeroPassword(newpass);
	}
	
	/**
	 * Change the PREFIX to detect commands.
	 * Example: <b>HERO PREFIX PhoneSQL#</b>
	 * 
	 * @param context is context of our application
	 * @param hero is the current commander
	 * @param argList are the argument list
	 */
	@SuppressWarnings("unused")
	private void onPREFIX(Context context, Hero hero, ArrayList<String> argList)
	{
		if( argList.size() < 1)
			return;
		
		String prefix = argList.get(0);
		List<Hero> heroList = Hero.listAll(Hero.class);
		
		for(Hero h : heroList)
			h.sendTextMessage( context.getString(R.string.hero_prefix_change, prefix) );
		
		AppConfig.getInstance(context).setMsgPrefix(prefix);
	}
}
