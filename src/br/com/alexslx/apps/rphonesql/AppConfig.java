package br.com.alexslx.apps.rphonesql;

import com.google.gson.Gson;

import br.com.alexslx.apps.rphonesql.models.LocationUpdate;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppConfig
{
	// Set false in production
	public static final boolean	LOG_DEBUG_ENABLED						= true;

	// Set to 1000 * 60 in production.
	public static final long	DELAY_MULTIPLIER						= 1000 * 60;

	// the minimum time interval for GPS notifications, in milliseconds
	// (default: 60000).
	public static final long	LOCATION_PROVIDERS_MIN_REFRESH_INTERVAL	= 60000;

	// the minimum distance interval for GPS notifications in meters.
	public static final float	LOCATION_PROVIDERS_MIN_REFRESH_DISTANCE	= 20;

	// max "age" in ms of last location (default 120000).
	public static final float	LAST_LOCATION_MAX_AGE					= 120000;

	// max "age" in ms to check if the location service is needed
	public static final long	LOCATION_SERVICE_CHECK_DELAY			= 30;

	/**
	 * Preferences UniqueIDS
	 */
	public static final String	PREFS_SMS_PREFIX						= "msg_prefix";
	public static final String	PREFS_CALL_LOUDER						= "call_louder";
	public static final String	PREFS_LAST_SHUTDOWN						= "last_shutdown";
	public static final String	PREFS_LAST_SIM							= "last_sim";
	public static final String	PREFS_HERO_PASSWD						= "hero_passwd";
	public static final String	PREFS_BLOCK_ALL							= "block_all";
	public static final String	PREFS_BLOCK_SMS							= "block_sms";
	public static final String	PREFS_BLOCK_CALL						= "block_call";
	public static final String	PREFS_ROUTEALL_COUNT					= "routeall_count";
	public static final String	PREFS_LOCATION_COUNT					= "location_count";
	public static final String	PREFS_LOCATION_INTERVAL					= "location_interval";
	public static final String	PREFS_LAST_LOCATION						= "last_location";

	private static AppConfig	_instance								= null;
	private Context				_context								= null;

	private String				msg_prefix								= "PhoneSQL#";
	private boolean				call_louder								= false;
	private String				last_shutdown							= "";
	private String				last_sim								= "";
	private String				hero_passwd								= "";
	private boolean				block_all								= false;
	private boolean				block_sms								= false;
	private boolean				block_call								= false;
	private int					routeall_count							= 0;
	private int					location_count							= 0;
	private long				location_interval						= 0;
	private LocationUpdate		last_location							= null;

	public static AppConfig getInstance(Context context)
	{
		if (_instance == null)
			_instance = new AppConfig(context);

		return _instance;
	}

	private AppConfig(Context context)
	{
		this._context = context;
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this._context);
		Gson gson = new Gson();

		this.msg_prefix = settings.getString(PREFS_SMS_PREFIX, "PhoneSQL#");
		this.call_louder = Boolean.valueOf(settings.getString(PREFS_CALL_LOUDER, "false"));
		this.last_shutdown = settings.getString(PREFS_LAST_SHUTDOWN, "N/A");
		this.last_sim = settings.getString(PREFS_LAST_SIM, "");
		this.hero_passwd = settings.getString(PREFS_HERO_PASSWD, "");
		this.block_all = Boolean.valueOf(settings.getString(PREFS_BLOCK_ALL, "false"));
		this.block_sms = Boolean.valueOf(settings.getString(PREFS_BLOCK_SMS, "false"));
		this.block_call = Boolean.valueOf(settings.getString(PREFS_BLOCK_CALL, "false"));
		this.routeall_count = Integer.valueOf(settings.getString(PREFS_ROUTEALL_COUNT, "0"));
		this.location_count = Integer.valueOf(settings.getString(PREFS_LOCATION_COUNT, "0"));
		this.location_interval = Long.valueOf(settings.getString(PREFS_LOCATION_INTERVAL, "0"));
		this.last_location = gson.fromJson(settings.getString(PREFS_LAST_LOCATION, ""), LocationUpdate.class);
	}

	/**
	 * Save an setting value into Android Preferences System
	 * 
	 * @param key
	 *            a unique string that identify this preference
	 * @param value
	 *            the value of this preference
	 */
	private void setConfigString(String key, String value)
	{
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this._context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(key, value);
		editor.commit();
	}

	/**
	 * Get our message prefix
	 */
	public String getMsgPrefix()
	{
		return this.msg_prefix;
	}

	/**
	 * Get current hero password
	 * 
	 * @return
	 */
	public String getHeroPassword()
	{
		return this.hero_passwd;
	}

	/**
	 * Should we active the external speaker in next call?
	 * 
	 * @return
	 */
	public boolean getLouderInNextCall()
	{
		return this.call_louder;
	}

	/**
	 * Get last shutdown date as a formated string
	 * 
	 * @return
	 */
	public String getLastShutdown()
	{
		return this.last_shutdown;
	}

	/**
	 * Get our SIM Serial
	 * 
	 * @return
	 */
	public String getSIMSerial()
	{
		return this.last_sim;
	}

	/**
	 * Check blocked states
	 * 
	 * @param type
	 *            of blocked parameter (ALL, SMS or CALL)
	 * @return the blocked state
	 */
	public boolean isBlocked(String type)
	{
		if (this.block_all)
			return this.block_all;

		if (type.equalsIgnoreCase("SMS"))
			return this.block_sms;

		if (type.equalsIgnoreCase("CALL"))
			return this.block_call;

		return false;
	}

	/**
	 * Get current count of routing all messages
	 * 
	 * @return the count
	 */
	public int getRouteAllCount()
	{
		return this.routeall_count;
	}

	/**
	 * Get current count of location tracks
	 * 
	 * @return the count
	 */
	public int getLocationCount()
	{
		return this.location_count;
	}

	/**
	 * Get current interval of location tracks
	 * 
	 * @return the count
	 */
	public long getLocationInterval()
	{
		return this.location_interval;
	}

	/**
	 * Get last known location
	 * 
	 * @return the location update object
	 */
	public LocationUpdate getLastLocation()
	{
		return this.last_location;
	}

	// ///////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Set our message prefix
	 * 
	 * @param data
	 *            is the new prefix
	 */
	public void setMsgPrefix(String arg)
	{
		this.msg_prefix = arg;
		this.setConfigString(PREFS_SMS_PREFIX, arg);
	}

	/**
	 * Set new hero password
	 * 
	 * @param passwd
	 *            is the new password
	 */
	public void setHeroPassword(String arg)
	{
		this.hero_passwd = arg;
		this.setConfigString(PREFS_HERO_PASSWD, arg);
	}

	/**
	 * Set to activate external speaker in next call
	 * 
	 * @param val
	 *            speaker needs to be activated
	 */
	public void setLouderInNextCall(boolean arg)
	{
		this.call_louder = arg;
		this.setConfigString(PREFS_CALL_LOUDER, Boolean.valueOf(arg).toString());
	}

	/**
	 * Set the last shutdown date
	 * 
	 * @param val
	 *            is last shutdown date formated
	 */
	public void setShutdown(String arg)
	{
		this.last_shutdown = arg;
		this.setConfigString(PREFS_LAST_SHUTDOWN, arg);
	}

	/**
	 * Sets our SIM Serial
	 * 
	 * @param val
	 *            is our SIM Serial
	 */
	public void setSIMSerial(String arg)
	{
		this.last_sim = arg;
		this.setConfigString(PREFS_LAST_SIM, arg);
	}

	/**
	 * Set blocked state of a parameter
	 * 
	 * @param type
	 *            of parameter (ALL, SMS or CALL)
	 * @param arg
	 *            the state (true or false)
	 */
	public void setBlocked(String type, boolean arg)
	{
		if (type.equalsIgnoreCase("ALL"))
		{
			this.block_all = arg;
			this.setConfigString(PREFS_BLOCK_ALL, Boolean.valueOf(arg).toString());
		}
		else if (type.equalsIgnoreCase("SMS"))
		{
			this.block_sms = arg;
			this.setConfigString(PREFS_BLOCK_SMS, Boolean.valueOf(arg).toString());
		}
		else if (type.equalsIgnoreCase("CALL"))
		{
			this.block_call = arg;
			this.setConfigString(PREFS_BLOCK_CALL, Boolean.valueOf(arg).toString());
		}
	}

	/**
	 * Set number of heros routing all sms
	 * 
	 * @param arg
	 *            number of heros routing this
	 */
	public void setRouteAllCount(int arg)
	{
		this.routeall_count = arg;
		this.setConfigString(PREFS_ROUTEALL_COUNT, String.valueOf(arg));
	}

	/**
	 * Set number of active location tracks
	 * 
	 * @param arg
	 *            number of active location tracks
	 */
	public void setLocationCount(int arg)
	{
		this.location_count = arg;
		this.setConfigString(PREFS_LOCATION_COUNT, String.valueOf(arg));
	}

	/**
	 * Set number of minimum interval of tracking
	 * 
	 * @param arg
	 *            number of minimum interval of tracking
	 */
	public void setLocationInterval(long arg)
	{
		this.location_interval = arg;
		this.setConfigString(PREFS_LOCATION_INTERVAL, String.valueOf(arg));
	}

	/**
	 * Set the last location object
	 * 
	 * @param lup
	 *            last location object
	 */
	public void setLastLocation(LocationUpdate lup)
	{
		Gson gson = new Gson();
		this.last_location = lup;
		this.setConfigString(PREFS_LAST_LOCATION, gson.toJson(lup, LocationUpdate.class));
	}
}
