package br.com.alexslx.apps.rphonesql.activities;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import br.com.alexslx.apps.rphonesql.R;

public class PhoneSQL extends Activity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		PackageManager p = getPackageManager();
		p.setComponentEnabledSetting(getComponentName(), PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
	}
}
