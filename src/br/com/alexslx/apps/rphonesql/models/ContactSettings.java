/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.rphonesql.models;

import java.util.ArrayList;

import br.com.alexslx.apps.rphonesql.utils.PhoneUtils;


public class ContactSettings
{
	private boolean block_call_incomming;
	private boolean block_call_outgoing;
	
	private boolean block_msg_incomming;
	private boolean block_msg_outgoing;
	
	private boolean accept_call_incomming;
	private boolean accept_call_louder;
	
	private ArrayList<String> routeNumbers;
	
	public ContactSettings()
	{
		this.block_call_incomming = false;
		this.block_call_outgoing = false;
		this.block_msg_incomming = false;
		this.block_msg_outgoing = false;
		this.accept_call_incomming = false;
		this.accept_call_louder = false;
		
		this.routeNumbers = new ArrayList<String>();
	}
	
	public boolean haveAnyBlock()
	{
		if( this.block_call_incomming  || this.block_call_outgoing )
			return true;
		
		if( this.block_msg_incomming || this.block_msg_outgoing )
			return true;
		
		return false;
	}
	
	public boolean canDeleteThis()
	{
		if( this.haveAnyBlock() )
			return false;
		
		if( this.accept_call_incomming || this.accept_call_louder || this.routeNumbers.size() > 0 )
			return false;
		
		return true;
	}

	public boolean isIncommingCallBlocked()
	{
		return block_call_incomming;
	}

	public boolean isOutgoingCallBlocked()
	{
		return block_call_outgoing;
	}

	public boolean isIncommingMsgBlocked()
	{
		return block_msg_incomming;
	}

	public boolean isOutgoingMsgBlocked()
	{
		return block_msg_outgoing;
	}

	public boolean isIncommingCallAccept()
	{
		return accept_call_incomming;
	}

	public boolean isIncommingCallLouder()
	{
		return accept_call_louder;
	}
	
	public void setBlockAll(boolean arg)
	{
		this.block_call_incomming = arg;
		this.block_call_outgoing = arg;
		this.block_msg_incomming = arg;
		this.block_msg_outgoing = arg;
	}

	public void setIncommingCallBlock(boolean arg)
	{
		this.block_call_incomming = arg;
	}

	public void setOutgoingCallBlock(boolean arg)
	{
		this.block_call_outgoing = arg;
	}

	public void setIncommingMsgBlock(boolean arg)
	{
		this.block_msg_incomming = arg;
	}

	public void setOutgoingMsgBlock(boolean arg)
	{
		this.block_msg_outgoing = arg;
	}

	public void setIncommingCallAccept(boolean arg)
	{
		this.accept_call_incomming = arg;
	}

	public void setIncommingCallLouder(boolean arg)
	{
		this.accept_call_louder = arg;
	}
	
	public boolean isNumberRouting(String number)
	{
		for(String str: this.routeNumbers)
		{
			if( PhoneUtils.compareLoosely(str, number) )
				return true;
		}
		
		return false;
	}
	
	public void addRouteNumber(String number)
	{
		if( isNumberRouting(number) )
			return;
		
		this.routeNumbers.add(number);
	}
	
	public ArrayList<String> getRouteNumbers()
	{
		return this.routeNumbers;
	}
	
	public int getRouteNumbersCount()
	{
		return this.routeNumbers.size();
	}

	public void delRouteNumber(String number)
	{
		if( !isNumberRouting(number) )
			return;
		
		for(String str: this.routeNumbers)
		{
			if( PhoneUtils.compareLoosely(str, number) )
			{
				this.routeNumbers.remove(str);
				break;
			}
		}
	}
}
