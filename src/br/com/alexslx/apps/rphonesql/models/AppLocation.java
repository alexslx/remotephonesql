package br.com.alexslx.apps.rphonesql.models;

import java.util.List;

import android.content.Context;
import br.com.alexslx.apps.rphonesql.utils.PhoneUtils;

import com.orm.SugarRecord;

public class AppLocation extends SugarRecord<AppLocation>
{
	private long interval;
	private String number;
	private String postURL;
	private long last_update;
	
	public AppLocation(Context context)
	{
		super(context);
		
		this.number			= "";
		this.postURL 		= "";
		this.interval		= 0;
		this.last_update 	= 0;
	}

	public long getInterval()
	{
		return interval;
	}

	public String getNumber()
	{
		return number;
	}

	public String getPostURL()
	{
		return postURL;
	}

	public void setInterval(long interval)
	{
		this.interval = interval;
	}

	public void setNumber(String number)
	{
		this.number = number;
	}

	public void setPostURL(String postURL)
	{
		this.postURL = postURL;
	}
	
	public long getLastUpdate()
	{
		return last_update;
	}

	public void setLastUpdate(int lastUpdate)
	{
		this.last_update = lastUpdate;
	}

	public static AppLocation getLocationByNumber(String number)
	{
		List<AppLocation> locList = AppLocation.listAll(AppLocation.class);
		for(AppLocation l : locList)
		{
			if( PhoneUtils.compareLoosely(number, l.getNumber()) )
				return l;
		}
		
		return null;
	}
	
	public static void onDeleteHero(String number)
	{
		AppLocation location = getLocationByNumber( number );
		if( location != null )
			location.delete();
	}
	
	public void onUpdateLocation(Context context, LocationUpdate lup)
	{
		long unixTime 	= System.currentTimeMillis();
		long diff		= unixTime - this.getLastUpdate();
		
		if( this.interval == 0 || diff >= this.interval )
		{
			PhoneUtils.sendMessage(this.number, lup.getMessage(context));
			
			if( this.interval > 0 )
			{
				this.last_update = unixTime;
				this.save();
			}
			else
			{
				try {
					Thread.sleep(2000);
				} catch( Exception e ){}
				
				PhoneUtils.deleteLocationListener(context);
				this.delete();
			}
		}
	}
}
