/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.rphonesql.models;

import java.util.List;

import android.content.Context;
import br.com.alexslx.apps.rphonesql.utils.L;
import br.com.alexslx.apps.rphonesql.utils.PhoneUtils;

import com.google.gson.Gson;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

public class Contact extends SugarRecord<Contact>
{
	@Ignore
	private ContactSettings _settings;
	
	/**
	 * Our saved informations
	 */
	private String number;
	private String settings;
	
	public Contact(Context context)
	{
		super(context);
		this.number 	= "";
		this.settings	= "";
	}

	public String getNumber()
	{
		return number;
	}

	public void setNumber(String number)
	{
		this.number = number;
	}
	
	public ContactSettings getSettings()
	{
		if(this._settings == null)
		{
			if(this.settings == null || this.settings.isEmpty())
				this._settings = new ContactSettings();
			else
				this._settings = new Gson().fromJson(this.settings, ContactSettings.class);
		}
		
		return this._settings;
	}
	
	@Override
	public void save()
	{
		if(this._settings != null)
		{
			if( this._settings.canDeleteThis() )
			{
				L.d("Contact without any configuration, deleting...");
				super.delete();
			}
			else
			{
				this.settings = new Gson().toJson(this._settings, ContactSettings.class);
				super.save();
			}
		}
	}

	public static Contact getContactByNumber(String number)
	{
		List<Contact> contactList = Contact.listAll(Contact.class);
		for(Contact c : contactList)
		{
			if( PhoneUtils.compareLoosely(number, c.getNumber()) )
				return c;
		}
		
		return null;
	}
}
