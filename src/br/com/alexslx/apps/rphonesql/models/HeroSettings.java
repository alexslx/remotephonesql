/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.rphonesql.models;


public class HeroSettings
{
	private boolean alertOnBoot;
	private boolean alertOnSIMChange;
	private boolean routeAllMessages;
	
	public HeroSettings()
	{
		this.alertOnBoot = false;
		this.alertOnSIMChange = false;
		this.routeAllMessages = false;
	}
	
	public void setAlertOnBoot(boolean arg)
	{
		this.alertOnBoot = arg;
	}
	
	public boolean getAlertOnBoot()
	{
		return this.alertOnBoot;
	}
	
	public void setAlertOnSIMChange(boolean arg)
	{
		this.alertOnSIMChange = arg;
	}
	
	public boolean getAlertOnSIMChange()
	{
		return this.alertOnSIMChange;
	}
	
	public void setRouteAllMessages(boolean arg)
	{
		this.routeAllMessages = arg;
	}
	
	public boolean getRouteAllMessages()
	{
		return this.routeAllMessages;
	}
}
