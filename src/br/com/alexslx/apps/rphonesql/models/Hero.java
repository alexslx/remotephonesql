/**
 * @author Alexandre Leites
 */
package br.com.alexslx.apps.rphonesql.models;

import java.util.List;

import android.content.Context;
import br.com.alexslx.apps.rphonesql.utils.PhoneUtils;

import com.google.gson.Gson;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

public class Hero extends SugarRecord<Hero>
{
	@Ignore
	private HeroSettings _settings;
	
	/**
	 * Our saved informations
	 */
	private String number;
	private String settings;
	
	public Hero(Context context)
	{
		super(context);
		this.number 	= "";
		this.settings	= "";
	}

	public String getNumber()
	{
		return number;
	}

	public void setNumber(String number)
	{
		this.number = number;
	}
	
	public HeroSettings getSettings()
	{
		if(this._settings == null)
		{
			if(this.settings == null || this.settings.isEmpty())
				this._settings = new HeroSettings();
			else
				this._settings = new Gson().fromJson(this.settings, HeroSettings.class);
		}
		
		return this._settings;
	}
	
	@Override
	public void delete()
	{
		AppLocation.onDeleteHero(getNumber());
		super.delete();
	}

	@Override
	public void save()
	{
		if(this._settings != null)
			this.settings = new Gson().toJson(this._settings, HeroSettings.class);
		
		super.save();
	}

	public static Hero getHeroByNumber(String number)
	{
		List<Hero> heroList = Hero.listAll(Hero.class);
		for(Hero h : heroList)
		{
			if( PhoneUtils.compareLoosely(number, h.getNumber()) )
				return h;
		}
		
		return null;
	}
	
	public static boolean isHero(String number)
	{
		Hero hero = getHeroByNumber(number);
		if( hero != null )
			return true;
		
		return false;
	}
	
	public void sendTextMessage(String text)
	{
		PhoneUtils.sendMessage(this.getNumber(), text);
	}
}
