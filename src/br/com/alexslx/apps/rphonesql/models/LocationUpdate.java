package br.com.alexslx.apps.rphonesql.models;

import java.util.Locale;

import br.com.alexslx.apps.rphonesql.R;
import android.content.Context;


public class LocationUpdate
{
	private double latitude;
	private double longitude;
	private float accuracy;
	private double altitude;
	private long timestamp;
	private float	speed;
	
	public LocationUpdate()
	{
		
	}

	public double getLatitude()
	{
		return latitude;
	}

	public double getLongitude()
	{
		return longitude;
	}

	public float getAccuracy()
	{
		return accuracy;
	}

	public double getAltitude()
	{
		return altitude;
	}

	public long getTimestamp()
	{
		return timestamp;
	}

	public void setLatitude(double latitude)
	{
		this.latitude = latitude;
	}

	public void setLongitude(double longitude)
	{
		this.longitude = longitude;
	}

	public void setAccuracy(float accuracy)
	{
		this.accuracy = accuracy;
	}

	public void setAltitude(double altitude)
	{
		this.altitude = altitude;
	}

	public void setTimestamp(long timestamp)
	{
		this.timestamp = timestamp;
	}
	
	public void setSpeed(float speed)
	{
		this.speed = speed;
	}
	
	public float getSpeed(float speed)
	{
		return this.speed;
	}
	
	public String getMessage(Context context)
	{
		return context.getString(R.string.location_message, String.format("%.8f", this.latitude), String.format("%.8f", this.longitude), (int)this.accuracy, (int)this.speed, this.getGMapsLink(this.latitude, this.longitude));
	}
	
	public String getGMapsLink(double latitude, double longitude)
	{
		return String.format(Locale.US, "http://maps.google.com/maps?q=loc:%.8f,%.8f", latitude, longitude);
	}
}
